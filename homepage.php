<?php $namePage="pageAccueil"; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Homepage | Vino Divino</title>
        <meta name="description" content="Homepge | Vino Divino" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
        <div id="wrapper">
            <?php include "header.php";?>
            <main id="homepage">
            	<div class="blocBienvenue">
					<h2 class="titre">Vino Divino <span>Antonio Visconti</span></h2>
					<h3 class="stitre"><span>Bar à vin</span> et restaurant italien</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt id est laborum. Aperiam, eaque ipsa quae ab illo explicabo.</p>
					<a href="#" class="link" title="En savoir plus">En savoir plus</a>
				</div>
				<div class="blocAtout clr">
					<div class="left"></div>
					<div class="right">
						<div class="slide-atout">
                            <div class="atout"><img src="images/atout1.svg" alt="Produits haut de gamme"><div><span>Produits</span> haut de gamme</div></div>
                            <div class="atout"><img src="images/atout2.svg" alt="Fraîcheur et travail artisanal"><div><span>Fraîcheur et</span> travail artisanal</div></div>
                            <div class="atout"><img src="images/atout3.svg" alt="En direct d’Italie"><div><span>En direct</span> d’Italie</div></div>
                            <div class="atout"><img src="images/atout4.svg" alt="Distributeur pour les restaurants"><div><span>Distributeur pour</span> les restaurants</div></div>
                            <div class="atout"><img src="images/atout5.svg" alt="Se déplace pour vos événements"><div><span>Se déplace pour</span> vos événements</div></div>
						</div>
					</div>
				</div>
				<div class="blocService">
					<h2 class="titre">Nos spécialités</h2>
					<div class="sous-service clr">
						<div class="left">
                            <a href="#" title="Bar à vin">
                                <span class="intro-service">
                                    <img src="images/icone-service.svg" alt="Bar à vin">
                                    <em>Bar à vin</em>
                                </span>
                                <span class="intro-service-hov">
                                    <strong class="title">Bar à vin</strong>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                                    <em class="plus"></em>
                                </span>
                            </a>
						</div>
						<div class="right">
                            <a href="#" title="Cuisine">
                                <span class="intro-service">
                                    <img src="images/icone-cuisine.svg" alt="Cuisine">
                                    <em>Cuisine</em>
                                </span>
                                <span class="intro-service-hov">
                                    <strong class="title">Cuisine</strong>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                                    <em class="plus"></em>
                                </span>
                            </a>
						</div>
					</div>
				</div>
				<div class="blocHoraire wrap clr">
					<div class="left">
						<span>Mercredi au vendredi — <em>11h</em> à <em>22h30</em></span>
						<span>Samedi — <em>11h</em> à <em>18h</em></span>
						<a href="#" class="link" title="Contactez-nous">CONTACTEZ NOUS</a>
					</div>
					<div class="right">
						<span><strong>Possibilité de <em>réservation</em></strong> en dehors des heures d’ouverture</span>
						<a href="#" class="link" title="Réserver">Réserver</a>
					</div>
				</div>
				<div class="blocEvent">
					<div class="wrap">
						<div class="sous-event">
							<h2 class="title">Soirée spéciale à thème</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore...</p>
							<a href="#" class="link" title="En savoir plus">En savoir plus</a>
						</div>
					</div>
				</div>
				<div class="blocGalerie">
					<h2 class="titre">Galerie photos</h2>
					<div class="slide-gal wrap">
						<div class="gal">
							<a href="#" title="">
								<img src="images/gal1.jpg" alt="Vino Divino"/>
								<span class="txt">Lorem ipsum dolor sit amet, consectetur adipisicing...</span>
								<span class="hide">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco laboris nisi ut consequat.</p>
									<em></em>
								</span>
							</a>
						</div>
						<div class="gal">
							<a href="#" title="">
								<img src="images/gal2.jpg" alt="Vino Divino"/>
								<span class="txt">Lorem ipsum dolor sit amet, consectetur adipisicing...</span>
								<span class="hide">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco laboris nisi ut consequat.</p>
									<em></em>
								</span>
							</a>
						</div>
						<div class="gal">
							<a href="#" title="">
								<img src="images/gal3.jpg" alt="Vino Divino"/>
								<span class="txt">Lorem ipsum dolor sit amet, consectetur adipisicing...</span>
								<span class="hide">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco laboris nisi ut consequat.</p>
									<em></em>
								</span>
							</a>
						</div>
						<div class="gal">
							<a href="#" title="">
								<img src="images/gal2.jpg" alt="Vino Divino"/>
								<span class="txt">Lorem ipsum dolor sit amet, consectetur adipisicing...</span>
								<span class="hide">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolore aliqua. Ut enim ad minim, quis nostrud exercitation ullamco laboris nisi ut consequat.</p>
									<em></em>
								</span>
							</a>
						</div>
					</div>
					<a href="#" class="link" title="Tous les photos">Tous les photos</a>
				</div>
            </main>
            <?php include "footer.php";?>
        </div>
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
			$(document).ready(function() {	
				// SLIDER
				$('#slider').slick({
					fade:true,
					dots: true
				});
                
				// SLIDE ATOUT
                slideAtout();
                $(window).resize(function() {
                    slideAtout();
                });
                function slideAtout(){
                    var w=$(window).width();
                     if (w>761){
                         if($('.slide-atout').hasClass('slick-initialized')){
                            $('.slide-atout').slick("unslick");
                         }
                     } else {
                         isInit=$('.slide-atout').hasClass('slick-initialized')
                         if(!isInit){
                            $('.slide-atout').slick({
                                dots:true,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                pauseOnHover:false,
                                focusOnSelect: true,
                                responsive: [
                                    { 
                                      breakpoint: 700,
                                      settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                      } 
                                    },
                                    { 
                                      breakpoint: 600,
                                      settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                      } 
                                    }
                                 ]
                            })
                        }
                     }
                 };

				// SLIDE GLERIE
                $('.slide-gal').slick({
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    responsive: [
                        {
                          breakpoint: 901,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                          }
                        },
                        { 
                          breakpoint: 601,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                          } 
                        }
                     ]
			    });
			});
        </script>
    </body>
</html>