<style>
/* FONT */
@font-face {
  font-family: akro-l;
  src:  url('fonts/akrobat-light.eot?#iefix') format('embedded-opentype'),  
        url('fonts/akrobat-light.otf')  format('opentype'),
	    url('fonts/akrobat-light.woff') format('woff'), 
        url('fonts/akrobat-light.ttf')  format('truetype'), 
        url('fonts/akrobat-light.svg#akrobat-light') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: akro-r;
  src:  url('fonts/akrobat-regular.eot?#iefix') format('embedded-opentype'),  
  		url('fonts/akrobat-regular.otf')  format('opentype'),
	    url('fonts/akrobat-regular.woff') format('woff'), 
	    url('fonts/akrobat-regular.ttf')  format('truetype'), 
	    url('fonts/akrobat-regular.svg#akrobat-regular') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: akro-s;
  src:  url('fonts/akrobat-semi-bold.eot?#iefix') format('embedded-opentype'),  
  		url('fonts/akrobat-semi-bold.otf')  format('opentype'),
	    url('fonts/akrobat-semi-bold.woff') format('woff'), 
	    url('fonts/akrobat-semi-bold.ttf')  format('truetype'), 
	    url('fonts/akrobat-semi-bold.svg#akrobat-semi-bold') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: akro-t;
  src:  url('fonts/akrobat-thin.eot?#iefix') format('embedded-opentype'),  
  		url('fonts/akrobat-thin.otf')  format('opentype'),
	    url('fonts/akrobat-thin.woff') format('woff'), 
	    url('fonts/akrobat-thin.ttf')  format('truetype'), 
	    url('fonts/akrobat-thin.svg#akrobat-thin') format('svg');
  font-weight: normal;
  font-style: normal;
}
    

@font-face {
  font-family: amari;
  src:  url('fonts/amarillo.eot?#iefix') format('embedded-opentype'),  
        url('fonts/amarillo.woff') format('woff'), 
        url('fonts/amarillo.ttf')  format('truetype'), 
        url('fonts/amarillo.svg#amarillo') format('svg');
  font-weight: normal;
  font-style: normal;
}

/* RESET */
body, ul, li, ol, form, h1, h2, h3, h4, h5, h6, div, span, p    { padding:0; margin:0; border:0; -webkit-text-size-adjust:none; -moz-text-size-adjust:none; text-size-adjust:none;}
article, side, footer, header, main, nav, section               { display:block;}  
input, textarea, select { -webkit-appernce:none; -ms-appernce:none; appernce:none; -moz-appernce:none; -o-appernce:none; border-radius:0;}
strong,b,em				{ font-weight:normal; font-family:akro-l; font-style: normal;}
ul						{ list-style-type:none;}
body					{ font:normal 16px/30px akro-l; color:#3c3b3b; background:#fff;}
a						{ text-decoration:none; color:#d3013; outline:none;}
img						{ border:none;}
#wrapper 				{ min-width:320px; overflow:hidden; position:relative;}
#wrapper *				{ box-sizing:border-box; outline:none;}
.wrap					{ max-width:1200px; width:100%; margin:0 auto; padding:0 40px; position:relative;} 

/* CLSS */
.cler			{ clear:both; float:none !important; width:100% !important; padding:0 !important; margin:0 !important; display:block;}
.left			{ float:left;}
.right 			{ float:right;}
.title 			{ font:normal 24px/30px amari; letter-spacing: .1px; color:#fff;}
.titre 		 	{ font:normal 24px/30px amari; letter-spacing: .1px; text-transform:capitlize; color:#0d0d0d; display: inline-block; padding: 0 110px; position: relative;}
.titre:before 	{ content: ''; background: #e5e5e5; width: 80px; height: 1px; position: absolute; top: 50%; left: 0; margin-top: 15px;}
.titre:after 	{ content: ''; background: #e5e5e5; width: 80px; height: 1px; position: absolute; top: 50%; right: 0; margin-top: 15px;}
.stitre, 
.sousTitre 		{ font:normal 24px/30px akro-r; color:#b34034; letter-spacing:.1px;}
.s-titre 		{ margin-bottom: 23px; font:normal 16px/30px akro-s; color: #9b170f; text-transform: uppercase; letter-spacing: .1px;}
.link 			{ display:inline-block; width:auto; height:60px; padding:14px 28px 0 28px; border: 1px #e5e5e5 solid; position:relative; font:normal 14px/30px akro-l; letter-spacing: .5px; color:#3c3b3b; text-decoration:none !important; text-transform:uppercase;}
.clr:after 		{ content:''; display:table; clear:both;}

/* HEDER */
#header					{ width:100%; position:relative;}
.headerTop				{ width:100%; position:relative; z-index:100;}
.top1 					{ height:64px; padding:0 60px;}
.slogan 				{ background: 0 50% no-repeat; height:60px; padding-top:17px; font:normal 18px/30px akro-l;}
.top1 .social 			{ float: right; padding-top: 13px;}
.top1 .social a			{ display:block; width:40px; height:40px; float: left; padding:0; margin-left:8px; text-indent:-9999px; cursor:pointer; border:1px #ccc solid; border-radius: 50%;}
.top1 .social .facebook	{ background:url(images/icone-facebook.svg) 50% no-repeat;}
.top1 .social .mail		{ background:url(images/icone-lettre.svg) 50% no-repeat;}
.top2 					{ background: #000; padding:0 60px; height:162px; position:relative;}
.headerTop .logo		{ display:block; width:223px; height:122px; position:absolute; left:40px; top:0; padding:14px 0 0 0; line-height:0; font-size:0; z-index:65;}
.headerTop .logo img	{ width:100%; height:auto;}
.headerTop .tel			{ width:auto; height:60px; padding:14px 29px 0 53px; font-size:16px; color:#fff; border: 1px #9b170f solid; position: absolute; top:52px; right: 60px; z-index: 75;}
.headerTop .tel:before  { content: ''; background:url(images/icone-tel.svg); width: 10px; height: 15px; position: absolute; top: 50%; left:29px; margin-top: -8px;}
.scroll-top 			{ display: block; background:url(images/icone-souris.svg) 50% 75% no-repeat; width: 30px; height: 45px; position: absolute; bottom: 30px; left: 0; right: 0; margin: 0 auto; z-index: 75;}
.scroll-top:before 		{ content: ''; background: #fff; width: 1px; height: 42px;  position: absolute; top: -60px; left: 0; right: 0; margin: 0 auto;}
.scroll-top:after 		{ content: ''; background: #fff; width: 1px; height: 20px;  position: absolute; bottom: -30px; left: 0; right: 0; margin: 0 auto;}

/* MENU */
.menu					{ width:100%; display:block; position:relative; z-index:60;}
.menu>ul				{ width:100%; display:block; position:relative; text-align:center; font-size:0; line-height:0; }	
.menu li				{ display:inline-block; position:relative; margin:0 20px;}	
.menu li:nth-child(8) 	{ margin-right:0 !important;}
.menu a					{ display:block; width:auto; height:162px; padding:0; position:relative; font:normal 14px/166px akro-t; color:#fff; text-transform: uppercase; letter-spacing: .75px;}
.menu>ul>li:hover>a:before{ content: ''; background: #9b170f; width: 1px; height: 42px; position: absolute; top: 23px; left: 0; right: 0; margin: 0 auto; opacity: 0;}
.menu>ul>li:hover>a:after { content: ''; background: #9b170f; width: 1px; height: 42px; position: absolute; bottom: 21px; left: 0; right: 0; margin: 0 auto; opacity: 0;}
.menu a span 			{ width:0; height:6px; position:absolute; left:0; bottom:0; background:#da3013; display:block;}
.menu>ul>li.active>a	{ color:#da3013;}
.menu>ul>li.active>a:before{ content: ''; background: #9b170f; width: 1px; height: 42px; position: absolute; top: 23px; left: 0; right: 0; margin: 0 auto;}
.menu>ul>li.active>a:after { content: ''; background: #9b170f; width: 1px; height: 42px; position: absolute; bottom: 21px; left: 0; right: 0; margin: 0 auto;}
.menu>ul>li.active>a span{ width:100%;}

/* SUB */
.sub					{ width:300px; position:absolute; top:162px; left:50%; z-index:995; margin-left:-150px; display:block; background:#fff; visibility:hidden; opacity:0;}
.sub li					{ width:100%; margin:0; border:none; padding:0; text-align:center;}
.sub li a				{ display:block; width:100%; height:60px; font:normal 16px/60px akro-t; color:#3c3b3b; letter-spacing: .75px; position:relative; border-bottom:1px solid #3c3b3b; margin:0;}
.sub li:last-child a	{ border:0;}
.menu li:hover .sub 	{ opacity:1; visibility:visible; z-index:999;}
.menu .sub li.active 	{ background:#9b170f;}
.menu .sub li.active a	{ color:#fff;}
.menu .social 			{ display:none; width:100%; padding:16px 40px; border-bottom:1px #000 solid;}
.menu .social a 		{ display:block; width:50px; height:50px; float:left; padding:0; margin-right:8px; text-indent:-9999px; cursor:pointer; border:none;}
.social .facebook		{ background:url(images/icone-facebook-hov.svg) 50% no-repeat #9b170f;}
.social .mail			{ background:url(images/icone-lettre-hov.svg) 50% no-repeat #9b170f;}

/* HIDE MOBILE */
.wrapMenuMobile, .menu 
	.vueMobile 			{ display:none;}
    
/* RESPONSIVE */
@media (min-width:1201px){ 
	.menu ul  							{ display:block !important;}
}
@media (max-width:1300px){
    .menu>ul>li				            { margin:0 15px;}
}
@media (max-width:1200px){
	.wrapMenuMobile						{ background: #9b170f; width:50%; height:70px; display:block; z-index:80; color:#fff; font:normal 16px/22px akro-l; cursor:pointer; padding:33px 20px 0 20px; text-transform:uppercase; position:absolute; left:0; top:-70px; z-index:105;}
	.menuMobile							{ width:25px; display:block; height:18px; padding-left:31px; cursor:pointer; position:relative; line-height:18px;}
	.menuMobile>div						{ width:20px; height:2px; background:#fff; position:absolute; left:0; top:50%; margin-top:-1px;}
	.menuMobile>div:before				{ width:100%; height:2px; background:#fff; position:absolute; left:0; top:8px; content:"";}
	.menuMobile>div:after				{ width:100%; height:2px; background:#fff; position:absolute; left:0; top:-8px; content:"";}
	.menuMobile.active>div				{ height:0px;}
	.menuMobile.active>div:before		{ top:0; transform:rotate(45deg);}
	.menuMobile.active>div:after		{ top:0; transform:rotate(-45deg);}
	.menu 								{ height:0;}
	.menu>ul 							{ width:100%; height:auto; position:absolute; left:0; top:0; padding:0; background:#fff; border-top: 1px #000 solid; z-index:999; display:none;}
	.menu>ul:after 						{ content: ""; width: 100%; height: 9999px; background: rgba(0,0,0,.6); position: absolute; left: 0; bottom: -9999px; z-index: -1;}
	.menu ul li 						{ width:100%; margin:0 auto; display:block; float:none; padding:0;}
	.menu>ul>li>a						{ height:53px; line-height:53px; border:0; padding:0 40px; margin:0; font-size:16px; text-align:left; color: #000; border-bottom:1px solid #000;}
	.menu>ul>li.active>a 				{ color:#9b170f;}
    .menu > ul > li.active > a::before,
    .menu > ul > li.active > a::after   { display: none;}
	.menu > ul > li.active > a span 	{ display: none;}
	.menu i 							{ display:block; width:100%; height:53px; position:absolute; right:0; top:0;}
	.menu i:before 						{ display:block; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat; transform:rotate(180deg); transition:all 400ms ease-in-out;}
	.menu i:after 						{ display:none; width:16px; height:8px; position:absolute; right:40px; top:50%; margin-top:-5px; content:""; background:url(images/icone-sub-menu.svg) 50% no-repeat; transition:all 400ms ease-in-out;}
	.menu i.active:before, .menu li.active i:before{ display:none;}
	.menu i.active:after, .menu li.active i:after{ display:block;}
	.menu .vueMobile 					{ display:block;}
	.menu .sub 							{ display:none; visibility:visible; width:100%; position:relative; left:auto; top:auto; margin:0; opacity:1; background:#000; padding:0; border-top:none;}
	.menu .sub li a						{ border-bottom:1px #cecece solid; font-size:16px; color:#fff; text-align:left; padding:0 40px;}
    .menu .sub li:last-child a,
    .menu li:hover .sub                 { border: none !important;}
    .menu .sub li.active a				{ background:#9b170f; color: #fff;}
	.menu .social 						{ display: block;}

	.top1 								{ height: 55px; padding: 0 40px;}
	.slogan, .top1 .social, .scroll-top	{ display:none;}
	.top2 								{ padding:0; height: 150px;}
	.headerTop .logo 					{ left: 0; top: 0; right: 0; margin: 0 auto; z-index:inherit;}
	.headerTop .tel 					{ height: 55px; padding: 13px 0 0 54px; color: #000; border: none; top: -56px; right: 40px;}
	.headerTop .tel::before 			{ content: ''; background: url(images/icone-tel2.svg); width: 15px; margin-top: -6px;}
}
@media (max-width:760px){
	.top1 								{ padding:0;}
}
@media (max-width:600px){	
	.wrapMenuMobile						{ padding:33px 20px 0 20px; width: 137px}
    .headerTop .logo                    { width: 267px; padding-top: 0; height: auto}
	.menu>ul>li> a, .menu .sub li a     { padding:0 20px;}
	.menu i::before, .menu i::after		{ right:20px;}
	.menu .social 						{ padding: 16px 20px;}
	.headerTop .tel 					{ right: 38px;}
}
	
/* Homepge */
<?php if ($namePage == "pageAccueil") { ?>
	/* BANNER */
	#slider						    { width:100%; height:540px; position:relative; z-index:45;}
	#slider .slick-list			    { width:100%; height:100%;}
	#slider .slick-track		    { width:100%; height:100%;}
	#slider .banner1			    { background:url(images/banner1-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
    #slider .banner2			    { background:url(images/banner2-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
    #slider .banner3			    { background:url(images/banner3-1800.jpg) 50% 50% no-repeat; -webkit-background-size:cover !important; -moz-background-size:cover !important; background-size:cover !important;}
	.txt-banner					    { background:rgba(0, 0, 0, 0.6); width: 545px; padding:138px 40px 0; position:absolute; right:0; top:0; bottom: 0; z-index:50; margin-top:-2px; text-align: center; color:#fff;}
	.slick-slide img 			    { display: block; height: 60px; margin: 0 auto; margin-bottom: 30px;}
	.txt-banner span 			    { display: block !important; margin-bottom: 20px; padding-bottom: 18px; font: 18px/30px amari; letter-spacing: .1px; color:#fff; text-transform: inherit; position: relative;}
	.txt-banner span:after 		    { content: ''; background: #9b170f; width: 42px; height: 1px; position: absolute; bottom: 0; left: 0; right: 0; margin: 0 auto;}
	.slick-prev 				    { background:url(images/icone-arrow-left.svg) 46% 50% no-repeat #f6f6f6; width:50px; height:50px; border:1px #dddcdc solid; border-radius: 50%; padding:0; margin-top:-35px; position:absolute; left:-35px; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none;}
	.slick-next 				    { background:url(images/icone-arrow-right.svg) 56% 50% no-repeat #f6f6f6; width:50px; height:50px; border:1px #dddcdc solid; border-radius: 50%; padding:0; margin-top:-35px; position:absolute; right:-35px; top:50%; z-index:50; cursor:pointer; text-indent:-9999px; outline:none;}
	.slick-dots 				    { width: 545px; height: 10px; margin-top: 0; position: absolute; padding: 0; line-height: 0; z-index: 50; right: 0; text-align: center; bottom: 135px;}
	.slick-dots	li				    { display:inline-block; height:12px; margin:0 6px; vertical-align: middle;}
	.slick-dots button			    { background:none; display:block; width:8px; height:8px; border-radius:50%; text-indent:-9999px; outline:none; padding:0; border:1px solid #fff; cursor:pointer; margin-top: 2px;}
	.slick-active button		    { background:#9b170f; width: 12px; height: 12px; border:1px solid #fff; margin-top: 0;}
	
    /* Content */
	.blocBienvenue 					{ max-width: 965px; width: 100%; margin: 0 auto; padding: 100px 40px; text-align: center;}
    .blocBienvenue .titre 			{ background: url(images/bg-titre.png) 50% 100% no-repeat; margin-bottom: 48px; padding-bottom: 30px; position: relative;}
    .blocBienvenue .titre:before,
    .blocBienvenue .titre:after 	{ margin-top: -20px;}
    .blocBienvenue .titre span 		{ display: block; margin-top: 4px; font: 14px/30px akro-l; color: #3c3b3b; text-transform: uppercase; text-align: center;}
    .blocBienvenue .stitre 			{ margin-bottom: 30px; text-transform: uppercase;}
    .blocBienvenue .link 			{ margin-top: 35px;}

    .blocAtout 						{ position: relative;}
    .blocAtout .left 				{ background: url(images/bg-atout.jpg) no-repeat; background-size: cover; width: 50%; position: absolute; top: 0; bottom: 0; left: 0;}
    .blocAtout .right 				{ width: 50%; height: 450px; border-bottom: 1px #e6e6e6 solid; border-top: 1px #e6e6e6 solid;}
    .slide-atout 					{ max-width: 335px; width: 100%; padding: 0; margin: 0 auto;}
    .atout 							{ height: 90px; padding: 33px 0 0 100px; border-bottom: 1px #e5e5e5 solid; position: relative; font: 16px/22px akro-l; letter-spacing: .5px; color: #3c3b3b;}
    .blocAtout .right>div>div:last-child { border: none;}
    .atout img 						{ position: absolute; top: 0; left: 10px; bottom: 0; margin: auto;}

    .blocService 					{ padding-top: 70px; text-align: center;}
    .sous-service 		            { width: 100%; height: 600px; margin-top: 70px; position: relative;}
    .sous-service a                 { display: block; width: 100%; height: 100%; position: relative; z-index: 50;}
    .blocService .left 				{ background: url(images/bg-service.jpg) no-repeat; background-size: cover; width: 50%; position: absolute; top: 0; bottom: 0; left: 0;}
    .intro-service                  { background: #9b170f; width: 310px; height: 310px; position: absolute; left: 0; top: 0; right: 0; bottom: 0; padding-top: 75px; margin: auto; opacity: 1;}
    .intro-service img 	            { display: block; margin: 0 auto; margin-bottom: 40px;}
    .intro-service em				{ display: inline-block; padding: 0 55px; position: relative; font:normal 24px/30px amari; letter-spacing: .1px; color:#fff;}
    .intro-service em:before 		{ content: ''; background: #fff; width: 42px; height: 1px; position: absolute; top: 50%; left: 0;}
    .intro-service em:after 		{ content: ''; background: #fff; width: 42px; height: 1px; position: absolute; top: 50%; right: 0;}
    .blocService .right 			{ background: url('images/bg-cuisine.jpg') no-repeat; background-size: cover; width: 50%; position: absolute; top: 0; bottom: 0; right: 0;}
    .intro-service-hov              { opacity: 0; width: 315px; height: 407px; padding: 90px 40px; position: absolute; top: 0; right: 0; bottom: 0; left: 0; margin: auto;}
    .intro-service-hov:before 	    { content: ''; background: #fff; width: 1px; height: 60px; position: absolute; top: 0; left: 0; right: 0; margin: 0 auto;}
    .intro-service-hov:after 	    { content: ''; background: #fff; width: 1px; height: 60px; position: absolute; bottom: 0; left: 0; right: 0; margin: 0 auto;}
    .intro-service-hov .title 		{ display: block; margin-bottom: 30px;}
    .blocService p, .sous-event p	{ color: #fff;}
    .blocService .plus 		        { display: block; background: url(images/icone-plus.svg) no-repeat; margin: 0 auto; width: 50px; height: 50px; margin-top: 27px;}

    .blocHoraire 					{ background: url(images/icone-heure.svg) 50% no-repeat; padding: 50px 85px 60px; text-align: center;}
    .blocHoraire.wrap               { max-width: 1110px}
    .blocHoraire span 				{ display: block;}
    .blocHoraire .right             { margin-top: 7px; margin-right: 31px;}
    .blocHoraire .left              { margin-top: 7px; margin-left: 10px;}
    .blocHoraire .left span:first-of-type { font-size: 18px;}
    .blocHoraire em 				{ font-style: normal; font:normal 18px/30px akro-s; letter-spacing: .5px; color: #9b170f;}
    .blocHoraire strong 			{ display: block; font:normal 18px/30px akro-s; letter-spacing: .5px;}
    .blocHoraire .link 				{ width: 150px; margin-top: 22px; padding: 13px 0 0;}

    .blocEvent 						{ background: url(images/bg-event.jpg) right center no-repeat #0b0300; background-size: contain; height: 305px; position: relative;}
    .blocEvent .wrap 				{ padding-top: 53px; text-align: right;}
    .sous-event 					{ background: url(images/icone-event.svg) 0 50% no-repeat; max-width: 515px; width: 100%; padding-left: 145px;}
    .sous-event p 					{ font-size: 16px; letter-spacing: .5px; text-align: left;}
    .sous-event .title 				{ margin-bottom: 22px; color: #9a8467; text-align: left;}
    .sous-event .link 				{ margin-top: 20px; color: #9b170f;}

    .blocGalerie 					{ padding: 70px 0 40px; text-align: center;}
    .slide-gal 						{ margin-top: 55px;}
    .gal 							{ width: auto; height: 313px; margin: 0 8px; overflow: hidden;}
    .gal a  						{ display: block; position: relative;}
    .gal a img 						{ display: block; width: 360px !important; height: 313px !important; margin-bottom: 0 !important;}
    .gal .txt		 				{ background: #262626; width: 100%; height: 75px; padding-top: 20px; position: absolute; left: 0; bottom: 0; z-index: 10; font-size: 12px; letter-spacing: .75px; color: #fff; opacity: 1;}
    .gal .hide 						{ background: rgba(38, 38, 38, .8); padding: 70px 40px 0; width: 100%; position: absolute; top: 0; bottom: 0; left: 0; right: 0; opacity: 0;}
    .gal .hide p 					{ font: 12px/24px akro-l; letter-spacing: .75px; color: #fff;}
    .gal .hide em 					{ display: block; background: url(images/icone-plus.svg) no-repeat; width: 50px; height: 50px; margin: 0 auto; margin-top: 25px;}
    .blocGalerie .link 				{ margin-top: 20px;}
	
/* RESPONSIVE */
@media (max-width:1300px){
	.slide-gal 							{ padding: 0 40px 70px;}
	.slick-prev 						{ margin-top: 0; left: 50%; margin-left: -60px; top: inherit; bottom: 0;}
	.slick-next 						{ margin-top: 0; right: 50%; margin-right: -60px; top: inherit; bottom: 0;}
}
@media (max-width:1200px){
	#slider 							{ height:450px; min-height:450px;}
	#slider .banner1					{ background:url('images/banner1-1200.jpg') 50% 50% no-repeat;}
    #slider .banner2					{ background:url('images/banner2-1200.jpg') 50% 50% no-repeat;}
    #slider .banner3					{ background:url('images/banner3-1200.jpg') 50% 50% no-repeat;}
	.txt-banner							{ width: 50%; padding: 85px 40px 0;}
	.slick-dots 						{ width: 50%; bottom: 110px;}
	.blocBienvenue 						{ padding: 65px 40px 60px;}
    .intro-service-hov, .gal .hide      { display: none !important;}
}
@media (max-width:1024px){	
    .gal .txt                           { padding: 18px 40px 0; line-height: 18px;}  
    .blocEvent .wrap                    { text-align: left;}
    .blocEvent                          { background-size: cover;}
    .sous-event .link                   { color: #fff;}
}
@media (max-width:900px){	
    .blocService 						{ padding-top: 55px;}
	.blocService .sous-service 			{ height: inherit; margin-top: 35px;}
	.blocService .sous-service>div 		{ height: 245px; position: relative;}
    .sous-service .left:after           { content: ''; background: rgba(155,23,15,.5); width: 100%; position: absolute; top: 0; right: 0; bottom: 0; left: 0;}
	.intro-service             			{ background: none; width: 100%; height: inherit; padding-top: 45px;}
    .intro-service a                    { z-index: 50;}
    .intro-service img 			        { margin-bottom: 39px;}
    .intro-service .title 			    { display: inline-block; margin-bottom: 0; padding: 0 55px; position: relative;}
	.intro-service .title:before 	    { content: ''; background: #fff; width: 39px; height: 1px; position: absolute; top: 50%; left: -3px; margin-top: 2px;}
	.intro-service .title:after 	    { content: ''; background: #fff; width: 39px; height: 1px; position: absolute; top: 50%; right: -3px; margin-top: 2px;}
    .sous-service .right 
    .intro-service img                  { margin-bottom: 31px;}
    .sous-service .right .intro-service 
    em:before                           { left: -21px; margin-top: 1px;}
    .sous-service .right .intro-service 
    em:after                            { right: -21px; margin-top: 1px;}
    .sous-service .right:after          { content: ''; background: rgba(0,0,0,.5); width: 100%; position: absolute; top: 0; right: 0; bottom: 0; left: 0;}
    .blocHoraire                        { padding: 48px 40px 60px;}
    
    .gal a 								{ height: 313px;}
    .gal a img                          { display: block; width: 100% !important; height: auto !important;}
    .gal .txt                           { padding: 28px 0px 0;} 
}
@media (max-width:760px){
	#slider 							{ height:305px; min-height:305px;}
	#slider .banner1					{ background:url('images/banner1-800.jpg') 50% 50% no-repeat;}
    #slider .banner2					{ background:url('images/banner2-800.jpg') 50% 50% no-repeat;}
    #slider .banner3					{ background:url('images/banner3-800.jpg') 50% 50% no-repeat;}
	.txt-banner 						{ background: rgba(0, 0, 0, 0.5); width: 100%; height: 170px; margin-top: 0; padding: 25px 40px 47px 40px; top: 50%; margin-top: -10px; text-align: center;}
	.txt-banner span 					{ margin-bottom: 0; padding-bottom: 0;}
	.txt-banner span::after,
	.txt-banner p 						{ display: none;}
	#slider .slick-slide img 			{ width: 100%; height: 50px;  margin: 0 auto; margin-bottom: 15px;}
	.slick-dots 						{ width: 100%; bottom: 25px;}
	
	.blocAtout .left 					{ width: 100%; height: 280px; position: relative;}
	.blocAtout .right 					{ width: 100%; height: 125px;}
	.slide-atout 						{ max-width: inherit!important; padding: 25px 40px 0;}
	.atout 								{ height: 40px !important; padding: 0; text-align: center;border: none;}
    .atout img                          { height: 40px !important; position: static; display: inline-block !important; vertical-align: middle; margin-right: 46px}
	.atout span 						{ display: block !important;}
    .atout div                          { display: inline-block; vertical-align:middle; text-align: left}
	.blocAtout .slick-dots 				{ bottom: -35px;}
	.blocAtout .slick-dots button 		{ border: 1px #ccc solid !important;}
    .blocHoraire>div                    { margin: 0 !important;}
    .gal .txt                           { padding: 18px 40px 0;}
    .gal a img                          { width: 360px !important;}
}
@media (max-width:600px) {	
	.titre 								{ max-width: 395px; width: 100%; padding: 0; font: normal 24px/44px amari;}
    .titre::before, .titre::after       { margin-top: 5px;}
	.blocBienvenue .titre 				{ margin-bottom: 37px;}
    .blocBienvenue .titre::before, 
	.blocBienvenue .titre::after		{ width: 40px !important; margin-top: -12px;}
	.blocBienvenue .stitre 				{ margin-bottom: 0; text-transform: uppercase;}
	.blocBienvenue .stitre span			{ display: block;}
	.blocBienvenue p 					{ display: none;}
	.slide-atout 						{ max-width: 225px;}	
	.blocService .left,
	.blocService .right 				{ width: 100%;}
	.blocHoraire 						{ background: none; padding: 0;}
	.blocHoraire>div 					{ width: 100%; float: none; padding: 50px 0 59px;}
    .blocHoraire .right                 { padding-bottom: 60px;}
	.blocHoraire .left 					{ border-bottom: 1px #e5e5e5 solid;}
    .blocHoraire .right                 { padding: 50px 60px;}
	.blocEvent 							{ background-size:cover;}
	.blocEvent .wrap 					{ background: rgba(20,7,0,.5); padding: 0; position: absolute; top: 0; right: 0; bottom: 0; left: 0; text-align: center;}
	.sous-event 						{ background: none; max-width: 100%; width: 72%; margin: 0 auto; padding-top: 57px; padding-left: 0;}
	.sous-event .title 					{ font: normal 24px/48px amari; text-align: center;}
	.sous-event p 						{ display: none;}
    .sous-event .link                   { margin-top: 9px;}
	
	.blocGalerie 						{ padding: 60px 0 40px;}
	.slide-gal 							{ margin-top: 30px; padding: 0;}
	.slide-gal .slick-prev 				{ background: url(images/icone-arrow-left.svg) 46% 50% no-repeat #262626 !important; width: 70px; height: 75px; border: none; border-radius:0; left: -8px; margin-left: 0; bottom:0;}
	.slide-gal .slick-next 				{ background: url(images/icone-arrow-right.svg) 46% 50% no-repeat #262626 !important; width: 70px; height: 75px; border: none; border-radius:0; right: -12px; margin-right: 0; bottom:0;}
	.gal 								{ margin: 0;}
    .gal .txt                           { padding: 28px 0px 0;}
	.gal a img 							{ width: 100% !important; height: auto !important;}
	.blocGalerie .link 					{ margin-top: 40px;}
}
@media (max-width:400px) {
	.blocBienvenue 						{ padding: 61px 0 60px;}
    .blocBienvenue .titre span          { margin-top: -3px;}
	.titre::before, .titre::after		{ width: 44px; margin-top: 12px;}
    .blocService .titre::before, 
    .blocService .titre::after          { width: 32px; margin-top: 3px;}
	.gal .txt 							{ padding: 18px 80px 0; line-height: 18px;}
    .blocAtout .left                    { background: url(images/bg-atout-mobile.jpg) no-repeat; background-size: cover;}
    .link                               { padding: 14px 29px 0 29px;}
    .blocGalerie .link                  { padding: 14px 37px 0 37px;}
    .blocHoraire .left span:first-of-type{ letter-spacing: 1px;}
    .blocHoraire strong                 { letter-spacing: 1px;}
}
<?php } ?>

/* Pges Intérieures */
<?php if ($namePage != "pageAccueil") { ?>
    .menu li:hover .sub     { border: 1px #000 solid; border-top: none;}
	.bredCrumb 				{ margin-bottom: 26px; padding-left: 0; border-bottom: 1px #ccc solid;}
	.bredCrumb a  			{ color: #262626; line-height: 60px; letter-spacing: .75px;}
	.bredCrumb span 		{ margin: 0 8px; font: normal 24px/30px akro-l; letter-spacing: .75px;}
	.bredCrumb .active 		{ color:#cb4349;}
	.titrePage	 			{ background: url('images/bg-titre.png') 50% 100% no-repeat; position: relative;font:normal 24px/30px amari; letter-spacing: .1px; text-transform:cpitlize; color:#0d0d0d; display: inline-block; margin-bottom: 35px; padding: 0 110px; padding-bottom: 34px;}
	.titrePage:before		{ content: ''; background: #e5e5e5; width: 80px; height: 1px; position: absolute; top: 50%; left: 0;}
	.titrePage:after		{ content: ''; background: #e5e5e5; width: 80px; height: 1px; position: absolute; top: 50%; right: 0;}
	.titrePage span 		{ display: block; margin-top: 4px; font: 14px/30px akro-l; color: #3c3b3b; text-transform: uppercase; text-align: center;}
	.sousTitre 				{ margin-bottom: 30px; text-transform: uppercase;}
    .chapo                  { font: normal 16px/30px akro-s; letter-spacing: .1px;}
	
	.blocTop 		        { padding: 60px 40px; text-align: center; max-width: 1010px; margin: 0 auto; width: 100%;}
	.blocPhoto 		        { width: 100%; min-height: 450px; border-bottom: 1px #e6e6e6 solid; border-top: 1px #e6e6e6 solid; position: relative; font-size: 0; line-height: 0; letter-spacing: 0;}
    .blocPhoto>div          { width: 50%; float: none; display: inline-block; vertical-align: middle;}
    .blocPhoto>div p 	    { font: 16px/30px akro-l; color: #3c3b3b;}
    .blocPhoto .left        { overflow: hidden;}
    .blocPhoto .left img    { display: block; width: 100%; height: auto;}
	.blocPhoto .right       { position: relative;}
	.blocPhoto .right>div 	{ width: 100%; padding: 45px 70px;}
	.blocPhoto .right>div 
    .sousTitre 		        { margin-bottom: 23px; font-size: 16px; letter-spacing: .1px;}
	
	.blocReserve 			{ background: url('images/bg-page-left.png') 0 50% no-repeat #000; height: 250px; margin: 80px 0 70px;}
	.blocReserve>div 		{ width: 50%; padding-top: 60px; text-align: center;}
	.blocReserve>div span  	{ display: block; font: 24px/30px akro-s; letter-spacing: .5px; color: #fff;}
	.blocReserve>div .link 	{ margin-top: 40px; padding: 13px 45px 0; color: #fff;}

	.blocText 		        { margin-bottom: 60px;}
    .blocText2              { margin-bottom: 0;}
    .blocText .left>ul      { padding-left: 29px;}
    .blocText .left>ul>li   { padding-left: 56px; margin: 20px 0; background: url('images/icone-liste.svg') left center no-repeat;}
    .blocText .left>ul
    >li:first-child         { margin-top: -5px;}
	.blocText .right        { margin-top: 10px;}

	.blocGalerie 			{ margin: 55px auto 80px; text-align: center;}
	.bloc 					{ width: 360px; height: 313px; float: left; margin-right:20px; overflow: hidden; position: relative;}
	.blocGalerie>div:last-of-type{ margin-right: 0;}
	.bloc a  				{ display: block; position: relative;}
	.bloc a img 			{ display: block; width: 360px !important; height: 313px !important;}
	.bloc .txt		 		{ background: #9b170f; width: 100%; height: 75px; padding-top: 22px; position: absolute; left: 0; right: 0; bottom: 0; z-index: 10; font-size: 18px; line-height: 24px letter-spacing: .75px; text-transform: uppercase; color: #fff; opacity: 1; text-align: center;}
	.bloc .hide 			{ background: rgba(38, 38, 38, .8); padding: 70px 40px 0; width: 100%; position: absolute; top: 0; bottom: 0; left: 0; right: 0; opacity: 0;}
	.bloc .hide strong 		{ font: normal 18px/24px akro-s; color: #fff; text-transform: uppercase;}
	.bloc .hide p 			{ font: 12px/24px akro-l; letter-spacing: .75px; color: #fff;}
	.bloc .hide em 			{ display: block; background: url(images/icone-plus.svg) no-repeat; width: 50px; height: 50px; margin: 0 auto; margin-top: 25px;}
	
	.pageLeft				{ position:relative; margin:0 auto; padding:7px 60px 73px 60px;}
		
/* RESPONSIVE */
@media (max-width:1366px) { 
    .blocPhoto .left img                { width: auto;}
}
@media (max-width:1200px) { 
    .bredCrumb                          { display: none;}   
    .bloc                               { width: 32%; margin-right: 2%;}
    .blocText .right                    { width: 50%; overflow: hidden;}
    .bloc .hide                         { display: none;}
}
@media (max-width:900px) { 
    .blocReserve                        { height: 217px; margin: 0; position: relative;}
    .blocReserve:before                 { content: ''; background: rgba(0, 0, 0, .8); position: absolute; top: 0; right: 0; bottom: 0; left: 0;}
    .blocReserve > div                  { width: 100%; padding: 34px 20px 0; float: none; position: relative;}
    .blocReserve > div .link            { margin-top: 25px;}
    .blocPhoto                          { min-height: inherit; height: auto; border: none;}
    .blocPhoto .left                    { width: 100%; float: none; position: relative; height: auto;}
    .blocPhoto .left img                { width: 100%; height: auto;}
    .blocPhoto .right                   { width: 100%; float: none; min-height: inherit;}
    .blocPhoto .right > div             { background: #e5e5e5; max-width: 100%; padding:30px 40px; border-top: 4px #fff solid;}
    .blocText                           { margin-bottom: 35px; padding: 50px 40px 0;}
    .blocText2                          { padding: 0 40px;}
    .blocText .left > ul > li           { padding-left: 38px;}
    .blocText .right                    { width: 100%;}
    .blocText .right img                { width: 100%; height: auto;}
    .blocGalerie                        { display: none;}
}
@media (max-width:600px) {
    .blocTop   			                { padding: 65px 0 50px;}
    .titrePage                          { padding-bottom: 32px; font: normal 24px/44px amari;}
    .titrePage span                     { margin-top: -6px;}
    .blocTop .sousTitre span            { display: block;}
    .chapo                              { padding: 0 20px;}
    .blocText                           { padding-left: 20px; padding-right: 20px;}
    .blocText2                          { padding: 0 20px;}
    .blocPhoto .right > div             { padding:30px 20px}
    .blocReserve                        { background: url('images/bg-page-left.png') 26% 50% no-repeat #000;}
    .blocText .left > ul                { margin-top: -15px;}
    .blocText .left > ul > li           { margin: 8px 0;}
    .blocText .right                    { margin-top: 30px;}
    .titrePage:before, .titrePage:after	{ width: 40px !important; margin-top: -13px;}
}
@media (max-width: 400px){
    .titrePage                          { width: 100%; padding: 0; padding-bottom: 32px;}
    .blocText .left > ul                { padding-left: 13px;}
}
<?php } ?>

/* FOOTER */
#footer						{ background:#000; width:100%; height:auto; position:relative;}
.footer 					{ padding:84px 40px;}
#footer div 				{ font-size: 14px; letter-spacing: .75px; color:#fff;}
.footer1 					{ text-align: center;}
.logo-foot					{ display:block; margin-bottom: 18px; font:normal 24px/30px amari; letter-spacing: .1px; color:#fff;}
.footer1 span               { padding-top: 9px; border-top: 1px #fff solid;}
.open-plan 					{ cursor:pointer; color:#fff;}

.footer2					{ margin-left:85px; font:normal 14px/30px akro-l; letter-spacing: .75px; color:#fff;}
.footer2 li 				{ display:inline-block; vertical-align: top; color:#fff;}
.footer2 li:nth-child(2) 	{ margin-left: 155px;}
.footer2 li:last-child 		{ margin-left: 95px;}
.footer2 li span 			{ display: block; font: 18px/30px akro-s; color: #9b170f;}
.footer2 a					{ color:#fff;}

.footer3 					{ margin-top:6px;}
.footer3 .rs				{ width:48px; height:48px; border: 1px #9b170f solid; border-radius:50%; text-indent:-9999px; display:block; cursor:pointer;}
.footer3 a 					{ display:block; float:left; margin:0 8px 0 0; cursor:pointer;}
.footer3 .rs.facebook		{ background:url(images/icone-facebook-hov.svg) 50% no-repeat;}
.rs.scroll 					{ background:url(images/icone-scroll.svg) 50% no-repeat;}

.toponweb					{ display:block; z-index:85; position:fixed; right:0; bottom:-65px; transition:bottom 400ms ease-in-out;}
.toponweb span				{ width:auto; height:60px; display:block; padding:20px; background:#7a120c;}
.toponweb img				{ width:74px; height:auto; display:block; margin:0 auto;}
.toponweb.show				{ bottom:-4px;}
.plan						{ width:100%; position:relative; padding:13px 60px; background:#fff; text-align:center; overflow:hidden; display:none;}
.plan a						{ padding:0 8px; color:#000;}

/* RESPONSIVE */
@media (max-width:1200px){
	.footer 							{ padding:45px 40px;}
	.footer2 li:nth-child(2),
	.footer2 li:last-child 				{ margin-left: 0;}
    .footer2 li:nth-child(1),
    .footer2 li:nth-child(2)            { margin-right: 50px;}
    
}
@media (max-width:980px) {
    .footer2                            { width: 54%; margin-left: 50px;}
    .footer2 li:nth-child(1),
    .footer2 li:nth-child(2)            { margin-right: 25px;}
    .footer3                            { position: absolute; right: 33px;}
}
@media (max-width:760px) {
	.footer 							{ padding:38px 20px 10px;}
	.footer>div 						{ width:100%; float:none; text-align: center;}
    .footer1 							{ margin-bottom: 0; float: none; text-align: center;}
	.logo-foot 							{ display: inline-block; margin-bottom: 3px;}
    .footer1 strong                     { margin-top: 10px; letter-spacing: 1px;}
	.scroll 							{ display: none !important;}
	.footer1 strong 					{ display: block;}
	.footer em, .open-plan				{ display: none !important;}
	.footer2 							{ width:100% !important; float: none; text-align: center;  margin:0 auto; margin-top:10px; margin-left: 0;}
	.footer2 li 						{ display: block;}
    .footer2 li:first-child             { margin-right: 0;}
	.footer2 li:nth-child(2), 
	.footer2 li:last-child 				{ width: 207px; height: 70px; margin: 0 auto !important; border: 1px #fff solid;}
	.footer2 li:nth-child(2) 			{ margin-top: 24px !important; margin-bottom: 20px !important;}
    .footer2 li:nth-child(2) span,
    .footer2 li:last-child span         { margin-bottom: -5px; padding-top: 6px;}
	.footer3 							{ float: left; margin-top:31px; position: relative; right: 0;}
    .footer3 .rs                        { background-color: #9b170f !important;}
}
@media (max-width:400px) {
	.footer2		 					{ width: 100% !important;}
	.footer2 li:first-child 			{ line-height:18px;}
    .footer2 li:first-child span        { margin-bottom: 1px;}
	.footer2 li span 					{ display:block;}
}

/* EFFET HOVER */
@media (min-width:1201px) {
	body a span, body a, span:before, span:after, a:after, a:before, .link, .slick-prev, .slick-next, .slick-dots button, .sub, .menu>ul>li>a:before, .menu>ul>li>a:after, .scroll-top, .blocService .right .title, .gal .txt, .gal .hide, .bloc .hide, .bloc .txt, .sous-service>div .intro-service, .sous-service>div .intro-service-hov { -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out;}
	a:hover, .blocAtout .right>ul
    >li:hover>a				            { color:#9b170f;}
	.logo:hover							{ opacity:.5;}
	.rs:hover 							{ background-color:#9b170f !important;}
	.rs.facebook:hover 					{ background: url(images/icone-facebook-hov.svg) 50% no-repeat;}
	.rs.mail:hover 						{ background: url(images/icone-lettre-hov.svg) 50% no-repeat;}
	.top1 .rs:hover						{ background-color: #9b170f !important; border-color: #9b170f;}
	.headerTop .tel:hover				{ background-color: #9b170f; border-color: #9b170f; color:#fff;}
	.menu li:hover>a 					{ color:#9b170f;}
	.menu>ul>li:hover>a:before 			{ opacity: 1;}
	.menu>ul>li:hover>a:after 			{ opacity: 1;}
	.menu .sub a:hover 					{ background:#9b170f; color:#fff;}
	.txt-banner a:hover 				{ color:#fff;}
	.txt-banner a:hover:after			{ width:0%;}
	.scroll:hover						{ background-color: #9b170f;}
	.scroll-top:hover					{ opacity: .5;}
    .scroll-top:hover:before            { opacity: 1 !important;}
    .scroll-top:hover:after             { opacity: 1 !important;}
	.link:hover 						{ background-color:#000; border-color:#000; color:#fff;}
    .sous-service>div:hover a           { background: #000;}
    .sous-service>div:hover .intro-service     { opacity:0;}
    .sous-service>div:hover .intro-service-hov { opacity:1;}
	.slick-prev:hover, 
	.slick-next:hover 					{  background-color:#000; border-color:#9c1316;}
	.sous-event .link:hover 			{ background-color: #fff; border-color: #fff; color: #9b170f;}
    .blocReserve .link:hover			{ background-color: #fff; border-color: #fff; color: #9b170f;}
	.gal:hover .txt, .bloc:hover .txt	{ opacity: 0 !important;}
	.gal:hover .hide, .bloc:hover .hide { opacity: 1;}
	.logo-foot:hover 				    { opacity: .5;}
	.footer .toponweb:hover span		{ background:#c3281e;}
}
	
/* SLICK */
.slick-slider					{ position:relative; display:block; -moz-box-sizing:border-box; box-sizing:border-box; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none; -webkit-touch-callout:none; -khtml-user-select:none; -ms-touch-action:pan-y; touch-action:pan-y; -webkit-tap-highlight-color:transparent;}
.slick-list						{ position:relative; display:block; overflow:hidden; margin:0; padding:0;}
.slick-list:focus				{ outline:none;}
.slick-list.dragging			{ cursor:pointer; cursor:hand;}
.slick-slider .slick-track,
.slick-slider .slick-list		{ -webkit-transform:translate3d(0 0,0); -moz-transform:translate3d(0,0,0); -ms-transform:translate3d(0,0,0); -o-transform:translate3d(0,0,0); transform:translate3d(0,0,0);}
.slick-track					{ position:relative; top:0; left:0; display:block;}
.slick-track:before, 
.slick-track:after 				{ display:table; content: '';}
.slick-track:after				{ clear:both;}
.slick-loading .slick-track		{ visibility:hidden;}
.slick-slide					{ display:none; float:left; height:100%; min-height:1px;}
[dir='rtl'] .slick-slide		{ float:right;}
.slick-slide img				{ display:block;}
.slick-slide.slick-loading img	{ display:none;}
.slick-slide.dragging img		{ pointer-events:none;}
.slick-initialized .slick-slide	{ display:block;}
.slick-loading .slick-slide		{ visibility:hidden;}
.slick-vertical .slick-slide 	{ display:block; height:auto; border:1px solid transparent;}
</style>