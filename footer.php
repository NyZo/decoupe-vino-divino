<!-- FOOTER START -->
<footer id="footer">
	<div class="footer wrap clr">
		<div class="footer1 left">
			<a href="homepage.php" class="logo-foot" title="Vino Divino"><img src="images/logo-foot.png" alt="Vino Divino"></a>
			<span><strong>BE 0848.694.372</strong> <em> | </em><a class="open-plan" title="Plan du site">Plan du site</a></span>
		</div>          
		<div class="footer2 left">
			<ul>
				<li><span>Adresse</span>Place Franco-Belge 2,<br/>6200 Châtelet</li>
				<li><span>Contact</span><a class="tel-foot" href="tel:0496/891638" title="0496 / 89 16 38">0496 / 89 16 38</a></li>
				<li><span>Mail</span><a href="mailto:vinodivino@vinodivino.be" title="mail">vinodivino@vinodivino.be</a></li>
			</ul>
		</div>          
		<div class="footer3 right clr">
			<a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook rs">Facebook</a>
			<a href="header" title="Allez vers le haut" class="scroll rs"></a>
		</div>
		<a href="http://www.toponweb.be/" title="Réalisé par Toponweb" target="_blank" class="toponweb">
			<span><img src="images/toponweb.svg" alt="Top On Web" /></span>
		</a>
	</div>
	<div class="plan">
	  <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
	  <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
	  <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
	  <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
	  <a href="#" title="Lorem Ipsum">Lorem Ipsum</a>
	</div>  
</footer>
<!-- FOOTER END -->