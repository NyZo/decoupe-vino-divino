<?php $namePage="page..."; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lng="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Page | Vino Divino</title>
        <meta nme="description" content="Page | Vino Divino" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->
        
        <?php include "css/css.php";?>
    </head>
    
    <body>
    	<div id="wrapper">
			<?php include "header.php";?>
           
            <main class="pageContent">
				<div class="bredCrumb wrap">
					<a href="homepage.php" title="Accueil">Accueil</a> <span>></span> <a href="page.php" class="active" title="Présentation">Présentation</a>
				</div>
				<div class="blocTop">
					<div class="titrePage">Présentation<span>Vino Divino</span></div>
					<h1 class="sousTitre"><span>Bar à vin</span> et restaurant italien</h1>
					<h2 class="chapo">Lorem ipsum dolor sit met, consectetur dipisicing elit, sed do eiusmod tempor incididunt ut lbore et dolore mgn liqu. Ut enim d minim venim, quis nostrud exercittion ullmco lboris. Duis ute irure dolor in reprehenderit in voluptte velit esse cillum dolore eu fugit null pritur. Excepteur sint occect cupidtt non proident, sunt in culp qui offici deserunt id est lborum. perim, eque ips que b illo explicbo.s</h2>
				</div>
				<div class="blocPhoto clr">
					<div class="left">
					    <img src="images/bg-page-left.jpg" alt="Vino Divino">
					</div>
					<div class="right">
						<div>
							<h2 class="s-titre">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</h2>
							<p>Lorem ipsum dolor sit met, consectetur dipisicing elit, sed do eiusmod tempor incididunt ut lbore et dolore mgn liqu. Ut enim d minim venim, quis nostrud exercittion ullmco lboris nisi ut liquip ex e commodo consequt. Duis ute irure dolor in reprehenderit in voluptte velit esse cillum dolore eu fugit null pritur. Excepteur sint occect cupidtt non proident, sunt in culp qui offici deserunt mollit nim id est lborum. Sed ut perspicitis unde omnis iste ntus error sit volupttem ccusntium doloremque ludntium, totm rem perim, eque ips que b illo inventore verittis et qusi rchitecto bete vite dict sunt explicbo.</p>
						</div>
					</div>
				</div>
				<div class="blocReserve clr">
					<div class="right">
						<span>Réservez une table maintenant !</span>
                        <a href="#" class="link" title="Réserver">Réserver</a>
					</div>
				</div>
				<div class="blocText wrap">
					<h2 class="s-titre">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</h2>
					<p>Consectetur dipisicing elit, sed do eiusmod tempor incididunt ut lbore et dolore mgn liqu. Ut enim d minim venim, quis nostrud exercittion ullmco lboris nisi ut liquip ex e commodo consequt. Duis ute irure dolor in reprehenderit in voluptte velit esse cillum dolore eu fugit null pritur. Excepteur sint occect cupidtt non proident, sunt in culp qui offici deserunt mollit nim id est lborum. Sed ut perspicitis unde omnis iste ntus error sit volupttem ccusntium doloremque ludntium, totm rem perim, eque ips que b illo inventore verittis et qusi rchitecto bete vite dict sunt explicbo.</p>
				</div>
				<div class="blocText blocText2 wrap clr">
					<div class="left">
						<h2 class="s-titre">Duis aute irure dolor in reprehenderit :</h2>
						<ul>
							<li>Excepteur sint occect non proident,</li>
							<li>Sunt in culp qui offici deserunt it nim,</li>
							<li>Sed ut perspicitis unde omnis iste ntus,</li>
							<li>Sit volupttem ccusntium doloremque,</li>
							<li>Totm rem perim, eque ips que b illo.</li>
						</ul>
					</div>
					<div class="right">
						<img src="images/img1.jpg" alt="Vino Divino">
					</div>
				</div>
				<div class="blocGalerie wrap clr">
					<div class="bloc">
						<a href="#" title="Découvrir notre concept">
							<img src="images/img2.jpg" alt="Découvrir notre concept">
							<span class="txt">Découvrir notre concept</span>
							<span class="hide">
								<strong>Découvrir notre concept</strong>
								<p>Sed do eiusmod tempor ut lbore et dolore liqu. Ut enim d minim, quis nostrud exercittion ullmco lboris nisi ut consequt.</p>
								<em></em>
							</span>
						</a>	
					</div>
					<div class="bloc">
						<a href="#" title="Nos prochaines soirées">
							<img src="images/img3.jpg" alt="Nos prochines soirées">
							<span class="txt">Nos prochaines soirées</span>
							<span class="hide">
								<strong>Nos prochaines soirées</strong>
								<p>Sed do eiusmod tempor ut lbore et dolore liqu. Ut enim d minim, quis nostrud exercittion ullmco lboris nisi ut consequt.</p>
								<em></em>
							</span>
						</a>	
					</div>
					<div class="bloc">
						<a href="#" title="Galerie photos">
							<img src="images/img4.jpg" alt="Galerie photos">
							<span class="txt">Galerie photos</span>
							<span class="hide">
								<strong>Galerie photos</strong>
								<p>Sed do eiusmod tempor ut lbore et dolore liqu. Ut enim d minim, quis nostrud exercittion ullmco lboris nisi ut consequt.</p>
								<em></em>
							</span>
						</a>	
					</div>
				</div>
            </main>
						
            <?php include "footer.php";?>
        </div>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
			$(document).ready(function() {
				// SERVICE
				$('.blocService').slick({
					dots:true,
					slidesToShow: 1,
					slidesToScroll: 1
				});
			});
        </script>
    </body>
</html>