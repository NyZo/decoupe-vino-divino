<!-- HEADER START -->
<header id="header" <?php if ($namePage != "pageAccueil") echo("class='page'"); ?>>
	<div class="headerTop">
		<div class="top1 clr">
			<?php if ($namePage == "pageAccueil") { ?>
        		<h1 class="slogan left">Bar à vin et restaurant italien à Châtelet</h1>
			<?php } else { ?>
                <div class="slogan left">Bar à vin et restaurant italien à Châtelet</div>
            <?php } ?>
			<div class="social clr">
				<a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook rs">Facebook</a>
				<a title="Envoyez-nous des e-mail" target="_blank" class="mail rs">E-mail</a>
			</div>
		</div>
		<div class="top2">
			<a href="homepage.php" class="logo" title="Vino Divino"><img src="images/logo.svg" alt="Vino Divino"/></a>
			<div class="wrapMenuMobile"><div class="menuMobile"><span>MENU</span><div></div></div></div>
			<nav class="menu">
				<ul>
					<li<?php if ($namePage == "pageAccueil") echo (" class='active'"); ?>><a href="homepage.php" title="Accueil">Accueil</a></li>
					<li<?php if ($namePage == "page...") echo (" class='active'"); ?>><a href="page.php" title="Présentation">Présentation</a></li>
					<li<?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="#" title="Bar à vin">Bar à vin</a><i></i>
						<ul class="sub">
							<li class="vueMobile<?php if ($namePage == "pageCon" && $nameSub == "pageNone") echo (" active"); ?>"><a href="#" title="Vue d'ensemble">Vue d'ensemble</a></li>
							<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="Page" title="Page">Page</a></li>
							<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?> class="active"><a href="Page" title="Page">Page</a></li>
							<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>>/l>><a href="Page" title="Page">Page</a></li>
							<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="Page" title="Page">Page</a></li>
						</ul>
					</li>
					<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Cuisine">Cuisine</a></li>
					<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Prochaines soirées">Prochaines soirées</a></li>
					<li<?php if ($nameSub == "pageCon1") echo (" class='active'"); ?>><a href="#" title="Galerie photos">Galerie photos</a></li>
					<li<?php if ($nameSub == "pageContact") echo (" class='active'"); ?>><a href="#" title="Contact">Contact</a></li>
					<li class="social clr">
						<a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook rs">Facebook</a>
						<a title="Envoyez-nous des e-mail" target="_blank" class="mail rs">E-mail</a>
					</li>
				</ul>
			</nav>
			<a href="tel:496891638" class="tel right" title="0496 / 89 16 38"><span>0496 / 89 16 38</span></a>
		</div>
		<?php if ($namePage == "pageAccueil") { ?>
			<div id="slider">
				<div class="banner1">
					<div class="txt-banner">
						<img src="images/icone-banner1.svg" alt="restaurant italien">
						<span>Restaurant italien</span>
						<p>Lors de soirées à thèmes,<br/>uniquement sur réservation</p>
					</div>
				</div>
				<div class="banner2">
					<div class="txt-banner">
						<img src="images/icone-banner2.svg" alt="restaurant italien">
						<span>Produits de bouche</span>
						<p>Pâtes artisanales, huile, vinaigre,<br/>truffe fraîche, charcuterie exportés d’Italie</p>
					</div>
				</div>
				<div class="banner3">
					<div class="txt-banner">
						<img src="images/icone-banner3.svg" alt="restaurant italien">
						<span>Bar à vin</span>
						<p>Vente de vins exclusifs de viticulteurs italiens<br/>et possibilité de dégustation sur place</p>
					</div>
				</div>
			</div>
			<a href="homepage" title="Allez vers le haut" class="scroll-top"></a>
		<?php } ?>
	</div>
<!-- HEADER END -->